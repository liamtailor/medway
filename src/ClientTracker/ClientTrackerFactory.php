<?php


namespace App\ClientTracker;


use App\Entity\Client;
use App\Repository\ClientRepository;
use Doctrine\Persistence\ManagerRegistry;

class ClientTrackerFactory
{
    public function make(ManagerRegistry $doctrine)
    {
        /** @var ClientRepository $repository */
        $repository = $doctrine->getRepository(Client::class);
        return new ClientTracker($repository, $doctrine->getManager());
    }
}