<?php


namespace App\ClientTracker;

use App\Repository\ClientRepository;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Client;

class ClientTracker
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var ObjectManager
     */
    private $entityManager;

    public function __construct(ClientRepository $clientRepository, ObjectManager $entityManager)
    {
        $this->clientRepository = $clientRepository;
        $this->entityManager = $entityManager;
    }

    public function getClient(string $sessionId): ?Client
    {
        $client = $this->clientRepository->findBy(['cookieTrackerCode' => $sessionId]);
        if (!$client) {
            $client = new Client();
            $client->setCookieTrackerCode($sessionId);
            $this->entityManager->persist($client);
            $this->entityManager->flush();
        }
        return $client;
    }
}