<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param int $pageNumber
     * @param int $productsPerPage
     * @return Product[] Returns an array of Product objects
     */
    public function findByPageNumber(int $pageNumber, int $productsPerPage)
    {
        $firstResult = (($pageNumber - 1) * $productsPerPage);
        return $this->createQueryBuilder('p')
            ->setFirstResult($firstResult)
            ->setMaxResults($productsPerPage)
            ->getQuery()
            ->getResult();
    }

    public function getCount()
    {
        return $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);
    }
}
