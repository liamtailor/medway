<?php

namespace App\Controller;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Entity\Client;
use App\Entity\Product;
use App\Entity\Warehouse;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class BasketController extends AbstractController
{
    /**
     * @Route("/basket/{clientId}", name="basket")
     */
    public function index($clientId)
    {
        $clientRepository = $this->getDoctrine()->getRepository(Client::class);
        $client = $clientRepository->find($clientId);
        $basket = $client->getBasket();
        return $this->render('basket/index.html.twig', [
            'basket' => $basket, 'clientId' => $clientId
        ]);
    }

    /**
     * @Route("/basket/add/{clientId}{productId}{quantity}", name="add_to_basket")
     */
    public function addToBasket($clientId, $productId, $quantity)
    {
        // TODO: Refaktoryzacja na obiekty.
        $productRepository = $this->getDoctrine()->getRepository(Product::class);
        /** @var Product $product */
        $product = $productRepository->find($productId);
        if (!$product) {
            new JsonResponse(['data' => ['error']]);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $clientRepository = $this->getDoctrine()->getRepository(Client::class);
        /** @var Client $client */
        $client = $clientRepository->find($clientId);

        $basket = $client->getBasket();
        if (!$basket) {
            $basket = new Basket();
            $client->setBasket($basket);
            $entityManager->persist($client);
        }

        $warehouseRepository = $this->getDoctrine()->getRepository(Warehouse::class);
        /** @var Warehouse $warehouse */
        $warehouse = $warehouseRepository->findOneBy(['Product' => $productId]);
        if (!$warehouse || $warehouse->getQuantity() <= 0){
            return new JsonResponse(['data' => ['error']]);
        } else {
            $warehouse->setQuantity($warehouse->getQuantity() + -$quantity);
            $entityManager->persist($warehouse);
        }

        $basketProduct = new BasketProduct();
        $basketProduct->setQuantity($quantity);
        $basketProduct->setProduct($product);
        $entityManager->persist($basketProduct);

        $basket->addBasketProduct($basketProduct);
        $entityManager->persist($basket);

        $entityManager->flush();

        return new JsonResponse(['data' => ['remainingQuantity' => $warehouse->getQuantity()]]);
    }

    /**
     * @Route("/basket/remove/{basketProductId}{quantity}", name="remove_from_basket")
     */
    public function removeFromBasket($basketProductId, $quantity)
    {
        $basketProductRepository = $this->getDoctrine()->getRepository(BasketProduct::class);
        $basketProduct = $basketProductRepository->find($basketProductId);
        if (!$basketProduct) {
            return; // TODO: Flash error?
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($basketProduct);

        $this->increaseProductQuantityInWarehouse($entityManager, $basketProductId, $quantity);

        $entityManager->flush();
        return new JsonResponse(['data' => ['test']]);
    }

    private function increaseProductQuantityInWarehouse(ObjectManager $manager, int $basketProductId, int $quantity)
    {
        $warehouseRepository = $this->getDoctrine()->getRepository(Warehouse::class);
        /** @var Warehouse $warehouse */
        $warehouse = $warehouseRepository->find($basketProductId);
        if (!$warehouse) {
            return; // TODO: Flash error?
        }
        $warehouse->setQuantity($warehouse->getQuantity() + $quantity);
        $manager->persist($warehouse);
    }
}
