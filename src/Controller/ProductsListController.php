<?php

namespace App\Controller;

use App\ClientTracker\ClientTrackerFactory;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class ProductsListController extends AbstractController
{
    /**
     * @Route("/products/list/{!page}", name="products_list")
     */
    public function index($page = 1)
    {
        /** @var Session $session */
        $session = $this->get('session');
        if (!$session->isStarted()) { // TODO: Sesja i tak jest resetowana po przejściu do koszyka i z powrotem.
            $session->start();
        }
        $sessionId = $session->getId();
        $productsPerPage = 4;
        /** @var ProductRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Product::class);
        $allProductsCount = $repository->getCount();
        $products = $repository->findByPageNumber($page, $productsPerPage);

        $clientTracker = (new ClientTrackerFactory())->make($this->getDoctrine());
        $client = $clientTracker->getClient($sessionId);
        return $this->render('products_list/index.html.twig', [
            'products' => $products, 'pagesCount' => ceil($allProductsCount / $productsPerPage),
            'currentPage' => $page, 'url' => 'products_list', 'clientId' => $client->getId()
        ]);
    }
}
