<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\Basket;
use App\Entity\Client;
use App\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * @Route("/order/{clientId}", name="order")
     */
    public function index(Request $request)
    {
        $clientId = $request->get('clientId');
        $clientRepository = $this->getDoctrine()->getRepository(Client::class);
        /** @var Client $client */
        $client = $clientRepository->find($clientId);
        if (!$client) {
            $client = new Client();
            $basket = new Basket();
            $client->setBasket($basket);
        }

        /** @var FormBuilderInterface $form */
        $formBuilder = $this->createFormBuilder($client);
        $form = $formBuilder->add('firstName', TextType::class, ['required' => true])
            ->add('lastName', TextType::class, ['required' => true])
            ->add('phoneNumber', TextType::class)
            ->add('email', EmailType::class)
            ->add('nip', NumberType::class, ['required' => true])
            ->add('street', TextType::class)
            ->add('town', TextType::class)
            ->add('postalCode', TextType::class)
            ->add('submit', SubmitType::class, ['label' => 'ZAMÓW'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();
            $order = new Order();
            $order->setClient($client);
            $order->setBasket($client->getBasket());  // TODO: setBasket niepotrzebne, bo przez klienta jest dostęp do koszyka.
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($client);
            $manager->persist($order);
            $manager->flush();
            return $this->redirect('/order-success');
        }

        return $this->render('order/index.html.twig', [
            'client' => $client, 'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/order-success", name="order-success")
     */
    public function success()
    {
        return $this->render('order/success.html.twig', [
        ]);
    }
}
