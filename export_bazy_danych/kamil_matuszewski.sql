-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Czas generowania: 09 Maj 2020, 13:50
-- Wersja serwera: 10.3.14-MariaDB
-- Wersja PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `kamil_matuszewski`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `basket`
--

DROP TABLE IF EXISTS `basket`;
CREATE TABLE IF NOT EXISTS `basket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `basket`
--

INSERT INTO `basket` (`id`) VALUES
(1),
(2),
(3),
(4),
(5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `basket_product`
--

DROP TABLE IF EXISTS `basket_product`;
CREATE TABLE IF NOT EXISTS `basket_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `basket_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_17ED14B44584665A` (`product_id`),
  KEY `IDX_17ED14B41BE1FB52` (`basket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `basket_product`
--

INSERT INTO `basket_product` (`id`, `product_id`, `quantity`, `basket_id`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 1, 1, 2),
(4, 1, 1, 3),
(5, 1, 1, 4),
(6, 1, 1, 5),
(7, 2, 1, 5),
(8, 2, 1, 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nip` int(11) DEFAULT NULL,
  `phone_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basket_id` int(11) DEFAULT NULL,
  `cookie_tracker_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C74404551BE1FB52` (`basket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `client`
--

INSERT INTO `client` (`id`, `first_name`, `last_name`, `nip`, `phone_number`, `basket_id`, `cookie_tracker_code`, `email`, `street`, `town`, `postal_code`) VALUES
(1, 'Jan', 'Kowalski', 12456, '10234', 1, '11c94ugoatrtkilkcljt9h0f68', 'jankowalksi@poczta.pl', '', '', ''),
(2, NULL, NULL, NULL, NULL, NULL, '35flikq68pe7lgs1eedkg2njjc', NULL, '', '', ''),
(3, NULL, NULL, NULL, NULL, 2, 'sqq579cckvj6s4eg8jvca0c3r0', NULL, '', '', ''),
(4, 'Jan', 'Kowalski', 123456, '654321', 3, 'q0rmccocj60c3f83n2ir01mkn1', 'jan@poczta.pl', 'Ulica 4/12', 'Miastko', '01-100'),
(5, 'Jan', 'Kowalski', 456465, '456465', 4, '50j8r34mnlus1mv2ckp7qgqsqg', 'asda@dsa', 'Ulica 4/12', 'Miastko', '01-100'),
(6, NULL, NULL, NULL, NULL, 5, '24990o3i6cian7hh4atbm43opb', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20200509083743', '2020-05-09 08:38:05'),
('20200509084444', '2020-05-09 08:44:50'),
('20200509093857', '2020-05-09 09:39:05'),
('20200509094225', '2020-05-09 09:42:29'),
('20200509094405', '2020-05-09 09:44:26'),
('20200509102729', '2020-05-09 10:31:16'),
('20200509124703', '2020-05-09 12:47:16'),
('20200509130710', '2020-05-09 13:09:07'),
('20200509132049', '2020-05-09 13:20:53'),
('20200509134013', '2020-05-09 13:40:23'),
('20200509134257', '2020-05-09 13:43:21'),
('20200509134732', '2020-05-09 13:47:40');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `basket_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F52993981BE1FB52` (`basket_id`),
  KEY `IDX_F529939819EB6921` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `order`
--

INSERT INTO `order` (`id`, `client_id`, `basket_id`) VALUES
(2, 4, 3),
(3, 5, 4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `product`
--

INSERT INTO `product` (`id`, `name`, `picture`, `price`, `currency`, `unit`) VALUES
(1, 'banana', 'banana.jpg', '10.00', 'pln', 'kg'),
(2, 'apple', 'banana.jpg', '10.00', 'pln', 'sztuka'),
(3, 'pineapple', 'banana.jpg', '5.00', 'pln', 'sztuka'),
(4, 'strawberry', 'banana.jpg', '7.00', 'pln', 'opakowanie 500g'),
(5, 'tomato', 'banana.jpg', '7.00', 'pln', 'opakowanie 1000g');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `warehouse`
--

DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE IF NOT EXISTS `warehouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_ECB38BFC4584665A` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `warehouse`
--

INSERT INTO `warehouse` (`id`, `product_id`, `quantity`) VALUES
(1, 1, 93),
(2, 2, 5),
(3, 5, 18);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `basket_product`
--
ALTER TABLE `basket_product`
  ADD CONSTRAINT `FK_17ED14B41BE1FB52` FOREIGN KEY (`basket_id`) REFERENCES `basket` (`id`),
  ADD CONSTRAINT `FK_17ED14B44584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Ograniczenia dla tabeli `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `FK_C74404551BE1FB52` FOREIGN KEY (`basket_id`) REFERENCES `basket` (`id`);

--
-- Ograniczenia dla tabeli `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `FK_F529939819EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `FK_F52993981BE1FB52` FOREIGN KEY (`basket_id`) REFERENCES `basket` (`id`);

--
-- Ograniczenia dla tabeli `warehouse`
--
ALTER TABLE `warehouse`
  ADD CONSTRAINT `FK_ECB38BFC4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
