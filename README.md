***Deploy:***
1. composer install
2. import bazy danych z folderu "/export-bazy-danych"
3. symfony server:start
4. http://localhost:8000/products/list

Żeby postwić na innym porcie: "cd public/" i "php -S localhost:8111"

***Połaczenie z bazą danych:***

DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/kamil_matuszewski"

Należy skonfigurować plik "/.env". Domyślny port dla bazy MySql to 3306, MariaDB 3307.
 